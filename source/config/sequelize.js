//Includes
var Sequelize = require('sequelize');
//exports
module.exports = function() {
    //Variables
    var controller = {};

    /**
     * getConfig
     * @return {Object} config
     */
    function getConfig() {
        //Variables
        var config = {
            host: 'localhost',
            database: 'controle',
            user: 'root',
            pass: 'root',
            adapter: 'mysql',
            logging: true
        };
        var env = process.env.NODE_ENV || 'development';
        //checking env
        if (env === "development") {
            //definition config
            config.host = '127.0.0.1';
            config.database = "yourdatabase";
            config.user = "root";
            config.pass = "root";
            config.logging = true;
        } else if (env === "test") {
            //definition config
            config.host = '127.0.0.1';
            config.database = "yourdatabase";
            config.user = "root";
            config.pass = "root";
            config.logging = true;
        } else if (env === "production") {
            //definition config
            config.host = '127.0.0.1';
            config.database = "databasename";
            config.user = "root";
            config.pass = "root";
            config.logging = false;
        }
        //return config
        return config;
    }

    /**
     * getConnection
     * @return {Object} connection
     */
    controller.getConnection = function(debug) {
        //Variables
        var config = getConfig();
        var connection = new Sequelize(config.database, config.user,config.pass, 
        {
            host: config.host,
            dialect: config.adapter,
            logging: config.logging
        });
        //return
        return connection;
    };

    //return
    return controller;
};