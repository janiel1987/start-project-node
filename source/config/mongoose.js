//includes
var Mongoose = require('mongoose');
//exports
module.exports = function() {
    //Variables
    var controller = {};

    /**
     * getConfig
     * @return {Object} config
     */
    function getConfig() {
        //Variables
        var config = {
            database: "databasename",
            host: 'localhost',
            user: 'root',
            pass: 'pass',
            port: 27017
        };
        var env = process.env.NODE_ENV || 'development';
        //checking env
        if (env === "development") {
            //definition config
            config.host = 'localhost';
            config.database = "databasename";
            config.user = "root";
            config.pass = "pass";
            config.port = 27017;
            console.log("Mongoose connected in development mode...");
        } else if (env === "test") {
            //definition config
            config.host = 'localhost';
            config.database = "databasename";
            config.user = "root";
            config.pass = "pass";
            config.port = 27017;
            console.log("Mongoose connected in test mode...");
        } else if (env === "production") {
            //definition config
            config.host = 'host';
            config.database = "databasename";
            config.user = "root";
            config.pass = "pass";
            config.port = 27017;
            console.log("Mongoose connected in production mode...");
        }
        //return config
        return config;
    };

    /**
     * getConnection
     * @return {Object} connection
     */
    controller.getConnection = function() {
        //Variables
        var config = getConfig();
        var connection = Mongoose.connect('mongodb://' + config.user + ':' + config.pass + '@' + config.host +':' + config.port + '/' + config.database, function(error){
            //check have error
            if (error) {
                //logger
                console.log(error);
            } else {
                //logger
                console.log("Connected to mongodb")
            }
        });
        //return 
        return connection;
    };

    //return
    return controller;
};