//includes
var nodemailer = require('nodemailer');
//exports
module.exports = function(app) {
    //Variables
    var controller = {};
    const _config = {
        host: 'yourdomain.com',
        port: 587,
        secure: false,
        tls: true,
        auth: {
            user: 'youremail@yourhost.com',
            pass: 'yourpassaccount'
        }
    };

    /**
     * send
     * @param {String} name
     * @param {String} address
     * @param {String} to
     * @param {String} replyTo
     * @param {String} subject
     * @param {String} message
     * @param {Array} attachments
     * @return {callback}
     */
    controller.send = function(name, to, replyTo, subject, message, attachments, cb) {
        //variables
        var mailOptions = {
            from: {
                name: name,
                address: _config.auth.user},
            to: to,
            replyTo: replyTo,
            subject: subject,
            text: '', //empty for send message with html
            html: message,
            attachments: attachments,
        };
        //set transporter
        transporter = nodemailer.createTransport(_config);
        // verify connection configuration
        transporter.verify(function(error, success) {
            if (error) {
                app.logger.log("error", "config - mail - send - " + error);
                cb(null);
            } else {
                //logger
                console.log('Server is ready to take our messages');
                //send mail
                transporter.sendMail(mailOptions, function(error, resp) {
                    //check error
                    if (error) {
                        //logger
                        app.logger.log("error", "config - mail - send - " + error);
                        cb(null);
                    } else {
                        //callback
                        cb(resp);
                    }
                });
            }
        });
    };

    //return
    return controller;
};