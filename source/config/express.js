//includes
var express = require("express");
var consign = require("consign");
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var helmet = require('helmet');
var i18n = require('i18n');
var cors = require('cors');
var logger = require("./logger")();
var sequelize = require("./sequelize")();
var mongoose = require("./mongoose")();
var utils = require("./utils");
var mail = require("./mail");
var expressValidator = require('express-validator');
//exports
module.exports = function() {
    //Variables
    var app = express();
    //set global Variables and configurations
    // set
    // Configure logger and debugging
    app.logger = logger.getLogger();
    app.debugging = logger.getDebug();
    //Include lib utils
    app.utils = utils(app);
    //create basic folders for logs
    app.utils.checkBasicFolders();
    //configure mail service
    app.mail = mail(app);
    //set port # CONFIGURE PORT FOR USING THIS APP
    app.set('port', 3000);
    //Configure connections database
    app.mysql = sequelize.getConnection();
    app.mongodb = mongoose.getConnection();
    //Configure enviroment variables
    app.env = process.env.NODE_ENV || 'development';
    app.debug = process.env.NODE_DEBUG || false;
    //Midleware
    app.use(express.static('./public'));
    //method bodyParser #IF NEED CONFIRE SIZE LIMIT FOR USING BODY PARSE DEFAULT 10MB
    app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
    app.use(bodyParser.json({limit: '10mb'}));
    app.use(require('method-override')());
    //Config cookie and session
    app.cookieParser = cookieParser;
    app.cookieSecret = "JF&(#H(SJSJJkjjh8h*SHHh199sjj9jjs9"; //Cookie secrect change if need
    app.cookieVersion = '1.0.4';
    app.use(app.cookieParser(app.cookieSecret));
    //Config session
    app.use(session(
        {
          secret: app.utils.randomString(25), //auto generate random string
          resave: true,
          saveUninitialized: true
        }
    ));
    app.use(expressValidator());
    //Configure i18n for multi language
    i18n.configure({
      locales: [
        'en-EU',
        'pt-BR',
        'es-SP'
      ],
        cookie: 'translatecookie',
        // controll mode on directory creation - defaults to NULL which defaults to umask of process user. Setting has no effect on win.
        directoryPermissions: '755',
        // whether to write new locale information to disk - defaults to true
        updateFiles: true,
        // sync locale information accros all files - defaults to false
        syncFiles: true,
        defaultLocale: 'en-EU',
        directory: './locales',
        autoReload: true,
        extension: '.json'
    });
    //initialize i18n for locales
    app.use(i18n.init);
    //Resolve problems with cors
    app.use(cors());
    //set express validator
    app.use(expressValidator());
    //Loading dependencies
    consign({cwd: 'app', verbose: false})
        .include("models")
        .then("controllers")
        .then("routes")
        .into(app);
    
    //Config error 404 for routes not configured and redirect for another url # CONFIGURE URL FOR REDIRECT
    app.get('*', function(req, res) {
        //redirect
        res.status(404).send('Page not found!');
    });

    //return
    return app;
};