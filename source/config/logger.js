//Includes
var winston = require('winston');
//module exports
module.exports = function() {
    //Variables
    var controller = {};

    /**
     * getLogger
     * @return {Object} logger
     */
    controller.getLogger = function() {
        //Variables
        var logger = new (winston.Logger)({
            transports: [
                new (winston.transports.Console)(),
                new (winston.transports.File)({filename: './logs/errors.log'})
            ]
        });
        //return
        return logger;     
    };

    /**
     * getDebug
     * @return {Object} logger
     */
    controller.getDebug = function() {
        //Variables
        var logger = new (winston.Logger)({
            transports: [
                new (winston.transports.Console)(),
                new (winston.transports.File)({filename: './logs/debug.log'})
            ]
        });
        //return
        return logger;
    };

    //Returns
    return controller;
};