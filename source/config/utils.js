//includes
var moment = require('moment');
var crypto = require('crypto');
var fs = require('fs');
//exports moment
module.exports = function(app) {
    //Variables
    var controller = {};
    var algorithm = 'aes-256-ctr';
    var hash = "jdafaj09ejfejfe9ajdf99ewjfejewjf9eea9fajds9j"
    /**
     * getRandomInt
     * @param min
     * @param max
     * @returns {*}
     */
    function getRandomInt(min, max) {
        //return
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    /**
     * checkBasicFolders
     */
    controller.checkBasicFolders = function() {
        //Variables
        var folders = ["locales","logs"];
        //check erros
        try {
            //checking
            for (var i = 0; i < folders.length; i++) {
                //check
                if (!fs.existsSync("./" + folders[i])) {
                    //create
                    fs.mkdirSync("./" + folders[i]);
                }
            }
            
        } catch (error) {
            //logger
            console.log(error);
        }
    };

    /**
     * decrypt
     * @param text
     * @returns {*}
     */
    controller.decrypt = function(text) {
        //Variables
        var decipher = crypto.createDecipher(algorithm, hash);
        var dec = decipher.update(text,'hex','utf8');
        //decipher
        dec += decipher.final('utf8');
        //return
        return dec;
    };
    /**
     * encrypt
     * @param text
     * @returns {*}
     */
    controller.encrypt = function(text){
        //Variables
        var cipher = crypto.createCipher(algorithm,hash);
        var crypted = cipher.update(text,'utf8','hex');
        //cipher
        crypted += cipher.final('hex');
        //return
        return crypted;
    };
    /**
     * randomString
     * @param len
     * @returns {string}
     */
    controller.randomString = function(len) {
        //buff
        var date = new Date();
        var buf = []
            , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' + date.getFullYear() + date.getMonth() + date.getDate() + date.getHours() + date.getHours() + date.getMinutes() + date.getSeconds()
            , charlen = chars.length;
        //list
        for (var i = 0; i < len; ++i) {
            buf.push(chars[getRandomInt(0, charlen - 1)]);
        }
        //return
        return buf.join('');
    };
    /**
     * validEmail
     * @param e-mail
     * @return boolean
     */
    controller.validEmail = function(email) {
        //Variables
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //check test
        if (regex.test(email)) {
            //return true
            return true;
        } else {
            //return false
            return false;
        }
    };
    /**
     * validDateYYYYMMDD
     * @param date
     * @return boolean
     */
    controller.validDateYYYYMMDD = function(date) {
        //Variables
        var regex = /\b\d{4}[-.]?\d{2}[-.]?\d{2}\b/;
        //check test
        if (regex.test(date)) {
            //return true
            return true;
        } else {
            //return false
            return false;
        }
    };
    /**
     * validDateDDMMYYYY
     * @param date
     * @return boolean
     */
    controller.validDateDDMMYYYY = function(date) {
        //Variables
        var regex = /\b\d{2}[/.]?\d{2}[/.]?\d{4}\b/;
        //check test
        if (regex.test(date)) {
            //return true
            return true;
        } else {
            //return false
            return false;
        }
    };
    /**
     * validPhoneNumber
     * @param phone
     * @return boolean
     */
    controller.validPhoneNumber = function(phone) {
        //Variables
        var regex = /\b\d{2}[-.]?\d{5}[-.]?\d{4}\b/;
        //check test
        if (regex.test(phone)) {
            //return true
            return true;
        } else {
            //return false
            return false;
        }
    };


   /**
    * replaceAll
    * @param {String} value
    * @param {String} search
    * @param {String} replace
    * @return {String} value
    */
    controller.replaceAll = function(value, search, replace) {
        //clear
        for (var i = value.length - 1; i >= 0; i--) {
            value = value.replace(search,replace);
        }
        //return
        return value;
    };

    /**
     * debugging
     * @param {String} message
     */
    controller.debugging = function(message) {
        //check debug
        if(app.debug) {
            //register
            app.debugging.log('info',message); 
        }
    }

    /**
     * checkPathConversionExists
     * @param {String} folder
     */
    controller.checkFolderExists = function(folder) {
        //try
        try {
            //check
            if (!fs.existsSync(folder)) {
                //debug
                controller.debugging("config - utils - checkFolderExists - create folder: " + folder);
                //create
                fs.mkdirSync(folder);
            }
        } catch (error) {
            //logger
            app.logger.log('error',"config - utils - checkFolderExists - " + error);
        }
    }

    /**
     * checkFileExists
     * @param {String} file
     * @return {Boolean} true/false
     */
    controller.checkFileExists = function(file) {
        //try
        try {
            //check
            if (fs.existsSync(file)) {
                //return
                return true;
            } else {
                //return
                return false;
            }
        } catch (error) {
            //logger
            app.logger.log("error", "config - utils - checkFileExists - " + error);
            return false;
        }
    }

    /**
     * deleteFolder
     * @param {String} path
     */
    controller.deleteFolder = function(path) {
        //try
        try {
            //check folder exists
            if(fs.existsSync(path)) {
                //list contents folders
                fs.readdirSync(path).forEach(function(file,index){
                    //create path
                    var curPath = path + "/" + file;
                    //check if path is directory
                    if(fs.lstatSync(curPath).isDirectory()) {
                        //recursive
                        controller.deleteFolder(curPath);
                    } else { 
                        // delete file
                        fs.unlinkSync(curPath);
                        //debug
                        controller.debugging("Removing: " + curPath);
                    }
                });
                //debug
                controller.debugging("Removing: " + path);
                //delete folder
                fs.rmdirSync(path);
            }
        } catch (error) {
            //logger
            app.logger.log("error","config - utils - deleteFolder - " + error);
        }
    }
    
    //return
    return controller;
};