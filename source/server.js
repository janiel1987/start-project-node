//includes
var app = require("./config/express")();
var http = require('http').createServer(app);
//Create server node
http.listen(app.get('port'), function() {
    //Initialize express
    console.log('Express Server listen: ' + app.get('port'));
});