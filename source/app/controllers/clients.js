//module
module.export = function(app) {
    //Variables
    var controller = {};
    var ClientsMySql = app.models.mysql.clients;
    var ClientsMongoDB = app.models.mongodb.clients;

    
    /**
     * listClientsMySql
     * @param {Object} req
     * @param {Object} res
     * @route /api/clients/mysql
     * @method GET
     */
    controller.listClientsMysql = function(req, res) {
        //query
        ClientsMySql.findAll().then((serverResponse) => {
            //response
            res.status(200).json(serverResponse);
        }, (error) => {
            //logger
            app.logger.log("error", "controllers - clients - listClientsMysql - " + error);
            //response
            res.status(500).json({message: "Internal server error"});
        });
    }

    /**
     * listClientsMongodb
     * @param {Object} req
     * @param {Object} res
     * @route /api/clients/mongodb
     * @method GET
     */
    controller.listClientsMongodb = function(req, res) {
        //query
        ClientsMongoDB.find((serverResponse) => {
            //response
            res.status(200).json(serverResponse);
        }, (error) => {
            //logger
            app.logger.log("error", "controllers - clients - listClientsMongodb - " + error);
            //response
            res.status(500).json({message: "Internal server error"});
        });
    }
}