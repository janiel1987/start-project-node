//module
module.exports = function(app) {
    //Variables
    var controller = app.controllers.clients;

    //route
    app.route('/api/clients/mysql')
        .get(controller.listClientsMysql);
    app.route('/api/clients/mongodb')
        .get(controller.listClientsMongodb);
}