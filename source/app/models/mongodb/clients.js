//Includes
var Mongoose = require('mongoose');
//exports
module.exports = function(app) {
    //Variables
    var schema = new Mongoose.Schema({
        id: {
            type: Number
        },
        active: {
            type: String
        },
        name: {
            type: String
        },
        phone: {
            type: Number
        }
    });
    //return
    return app.mongodb.model('clients', schema);
};