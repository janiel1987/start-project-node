//Includes
var Sequelize = require('sequelize');
//Module exports
module.exports = function(app) {
    //Declare model
    var Model = app.mysql.define('clients',{
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
      },
      active: {
        type: Sequelize.STRING(1),
        allowNull: false,
        defaultValue: 'Y'
      },
      name: {
        type: Sequelize.STRING(80),
        allowNull: false
      },
      phone : {
        type: Sequelize.INTEGER,
        allowNull: false
      }
    },{
      //Desable fields for not create automatic into table
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true,
      tableName: 'clients'
    });
    //Return
    return Model;
};