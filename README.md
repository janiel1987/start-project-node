### start project NODE JS with EXPRESS SEQUELIZE and MONGOOSE.
recomended for website, if need api rest this example not recomended.

# Structure folders
- source
    - app (Backend api)
        - controllers
        - models
        - routes
    - config (Libs and config files)
    - dev (Frontend development)
        - public
            - css
            - js
            - img
    - public (Frontend build)
    - tests (Tests folders)
        - api (Tests backup using frisby)

# Use e-mail send
    Access folder source/config/mail.js, edit config e-mail. Use code below:

    '
        //with attacments
        var attach = [
            {
                name: 'my attach',
                file: fs.createReadStream('/text.txt')
            }
        ];

        //send mail
        app.mail.send(
            'name', 
            'destiny@host.com', 
            'replytoanotherdestiny@host.com',
            'subject', 
            'you text e-mail',
            attach,
            (response) => {
                //log response
                console.log(response);
            });
    '